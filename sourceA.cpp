#include "sqlite3.h"
#include <string>
#include <iostream>

using namespace std;

int main(void)
{
	int rc;
	sqlite3* db;
	char *ErrorMsg = 0;
	
	rc = sqlite3_open("FirstPart.db", &db);

	if (rc)
	{
		cout << endl << "sorry I can't open your file..." << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		return(1);
	}

	rc = sqlite3_exec(db, "CREATE table people(ID integer autoincrement, name VARCHAR)", NULL, 0, &ErrorMsg);

	if (rc != SQLITE_OK)
	{
		cout << endl << "sorry I can't create a table..." << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		return(1);
	}

	rc = sqlite3_exec(db, "INSERT INTO people(name) values('kofiko')", NULL, 0, &ErrorMsg);

	if (rc != SQLITE_OK)
	{
		cout << endl << "sorry I can't create a table..." << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		return(1);
	}

	rc = sqlite3_exec(db, "INSERT INTO people(name) values('Harry Potter')", NULL, 0, &ErrorMsg);


	if (rc != SQLITE_OK)
	{
		cout << endl << "sorry I can't create a table..." << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		return(1);
	}

	rc = sqlite3_exec(db, "UPDATE people SET name='Mag' WHERE name='Harry Potter'", NULL, 0, &ErrorMsg);

	sqlite3_close(db);

	system("pause");
	return(0);
}

