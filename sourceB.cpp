#include "sqlite3.h"
#include <string>
#include <iostream>

using namespace std;

int callback(void *data, int argc, char **argv, char **azColName);
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg);
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg);

int main(void)
{
	int rc;
	sqlite3* db;
	char *ErrorMsg = 0;
	bool success;

	rc = sqlite3_open("carsDealer.db", &db);

	if (rc)
	{
		cout << endl << "sorry I can't open your file..." << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		return(1);
	}

	success = carPurchase(1, 1, db, ErrorMsg); //unsuccess.
	success = carPurchase(12, 4, db, ErrorMsg); // success.
	success = carPurchase(1, 4, db, ErrorMsg); // success.

	system("pause");
}

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	int rc, ava, carPrice, balance;
	char *ErrorMsg = 0;
	char* save;
	char* sql;
	const int ZERO = 0, ONE = 1;
	bool ret;

	itoa(carid, save, 10);
	strcpy(sql, "SELECT available FROM cars WHERE id=");
	strcat(sql, save);
	strcat(sql, ";");

	rc = sqlite3_exec(db, sql, callback, &ava, &ErrorMsg);

	if (rc)
	{
		cout << endl << "sorry I can't open your file..." << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		return(1);
	}

	if (ava != ONE)
	{
		ret = false;
	}

	else
	{
		strcpy(sql, "SELECT price FROM cars WHERE id=");
		strcat(sql, save);
		strcat(sql, ";");

		rc = sqlite3_exec(db, sql, callback, &carPrice, &ErrorMsg);

		if (rc)
		{
			cout << endl << "sorry I can't open your file..." << sqlite3_errmsg(db) << endl;
			sqlite3_close(db);
			return(1);
		}

		itoa(buyerid, save, 10);
		strcpy(sql, "SELECT balance FROM accounts WHERE id=");
		strcat(sql, save);
		strcat(sql, ";");

		rc = sqlite3_exec(db, sql, callback, &balance, &ErrorMsg);

		if (rc)
		{
			cout << endl << "sorry I can't open your file..." << sqlite3_errmsg(db) << endl;
			sqlite3_close(db);
			return(1);
		}

		if ((balance - carPrice) < ZERO)
		{
			ret = false;
		}

		else
		{
			ret = true;
		}
	}

	return(ret);
}

int callback(void *data, int argc, char **argv, char **azColName)
{
	string value;

	*static_cast<int*>(data) = atoi(argv[0]);

	return(0);
}

bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	int rc, fromMoney, toMoney;
	char *ErrorMsg = 0;
	char* save;
	char* sql;
	const int ZERO = 0, ONE = 1;
	bool ret;

	itoa(from, save, 10);
	strcpy(sql, "SELECT balance FROM accounts WHERE id=");
	strcat(sql, save);
	strcat(sql, ";");

	rc = sqlite3_exec(db, sql, callback, &fromMoney, &ErrorMsg);

	if ((fromMoney - amount) < ZERO)
	{
		ret = false;
	}

	else
	{
		strcpy(sql, "UPDATE accounts SET balance=");
		itoa((fromMoney - amount), save, 10);
		strcat(sql, save);
		strcat(sql, " WHERE id=");
		itoa(from, save, 10);
		strcat(sql, save);
		strcat(sql, ";");

		rc = sqlite3_exec(db, sql, NULL, 0, &ErrorMsg);

		if (rc)
		{
			cout << endl << "sorry I can't open your file..." << sqlite3_errmsg(db) << endl;
			sqlite3_close(db);
			return(1);
		}

		itoa(to, save, 10);
		strcpy(sql, "SELECT balance FROM accounts WHERE id=");
		strcat(sql, save);
		strcat(sql, ";");

		rc = sqlite3_exec(db, sql, callback, &toMoney, &ErrorMsg);

		if (rc)
		{
			cout << endl << "sorry I can't open your file..." << sqlite3_errmsg(db) << endl;
			sqlite3_close(db);
			return(1);
		}

		strcpy(sql, "UPDATE accounts SET balance=");
		itoa((toMoney + amount), save, 10);
		strcat(sql, save);
		strcat(sql, " WHERE id=");
		itoa(to, save, 10);
		strcat(sql, save);
		strcat(sql, ";");

		rc = sqlite3_exec(db, sql, NULL, 0, &ErrorMsg);

		if (rc)
		{
			cout << endl << "sorry I can't open your file..." << sqlite3_errmsg(db) << endl;
			sqlite3_close(db);
			return(1);
		}

		ret = true;
	}

	return(ret);
}